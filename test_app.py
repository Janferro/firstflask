import pytest
from app import FourData, app, Base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, Sequence
from werkzeug.wrappers import Response
import requests
import csv
from io import StringIO
import json




def test_new_fourdata():

    fourd = FourData(name='Marcin', city='Szczecin', title="Zielone pole", completed=False)
    assert fourd.name == 'Marcin'
    assert fourd.city == 'Szczecin'
    assert fourd.title == 'Zielone pole'
    assert fourd.completed == False


def test_home_page():

    flask_app = app

    # Create a test client using the Flask application configured for testing
    with flask_app.test_client() as test_client:
        response = test_client.get('/app/user_task')
        assert response.status_code == 200
        response = test_client.post('/')
        assert response.status_code == 404




"""@pytest.fixture
def client():

    flask_app=app
    flask_app.config["TESTING"] = True
    flask_app.testing = True

    # This creates an in-memory sqlite db
    # See https://martin-thoma.com/sql-connection-strings/

    engine = create_engine('sqlite:///db')
    Session = sessionmaker(bind=engine)

    Base.metadata.create_all(engine)

    # connecting to db
    session = Session()

    flask_app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    client = flask_app.test_client()
    with app.app_context():
        #db.create_all()
        fourd1 = FourData(name='Marcin', city='Szczecin', title="Zielone pole", completed=False)
        session.add(fourd1)
        session.commit()
    yield client"""


