# firstflask
https://jsonplaceholder.typicode.com/ wystawia sztuczne api do testowania prototypów.

1. pobiera dane z 2 zasobów wystawianych przez tą stronę
users - listę danych o użytkownikach
todos - listę danych o zadaniach

2. zapisuje pobrane dane w bazie SQLLite

3. wystawia endpoint, który zwraca plik csv z listą użytkowników wraz z przypisanymi im
zadaniami. Każdy wiersz zawiera kolumny:
• name
• city
• title
• completed
Dane te sa zaczytywane ze stworzonej bazy.
Wystawiony endpoint app/user_task na localhost na porcie 8080.


