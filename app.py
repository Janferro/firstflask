import requests
from requests import HTTPError, RequestException, ConnectionError
import csv
from io import StringIO
from flask import Flask
from werkzeug.wrappers import Response
from sqlalchemy import Column, Integer, String, BOOLEAN
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, Sequence


app = Flask(__name__)
Base = declarative_base()


class FourData(Base):
    __tablename__ = "fourdata"
    id = Column(Integer, Sequence('fourdata_id_seq'), primary_key=True)
    name = Column(String(50))
    city = Column(String(50))
    title = Column(String(50))
    completed = Column(BOOLEAN, default=False)

    def __repr__(self):
        return "<FourData(name='%s')>" % self.name


# create an engine that stores data in the local directory's
# db file.
engine = create_engine('sqlite:///db')
Session = sessionmaker(bind=engine)

Base.metadata.create_all(engine)

# connecting to db
session = Session()


@app.route('/app/user_task')
def download_app():
    def generate():
        # retrieving data from the web

        try:
            r1 = requests.get("https://jsonplaceholder.typicode.com/users")
            r2 = requests.get("https://jsonplaceholder.typicode.com/todos")

        except ConnectionError as e:
            print("ConnectionError !!!. Something went wrong. Check your web address")
        except HTTPError as e:
            print("HTTPError!!!")
        except RequestException:
            print("RequestException. Something wrong occurred while handling your request!!!")

        r1 = r1.json()
        r2 = r2.json()

        # placing data in db
        for i in r1:
            for j in range(len(r2)):
                if i["id"] == r2[j]["userId"]:
                    fourdata = FourData(name=i["name"], city=i["address"]["city"], title=r2[j]["title"],
                                        completed=r2[j]["completed"])
                    session.add(fourdata)
                    session.commit()

        data = StringIO()
        w = csv.writer(data)

        # Creating the csv file
        # write header
        w.writerow(('NAME', 'CITY', 'TITLE', 'COMPLETED'))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)

        # write data from db
        for instance in session.query(FourData).order_by(FourData.id):
            w.writerow((instance.name, instance.city, instance.title, instance.completed))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)

    # stream the response as the data is generated
    response = Response(generate(), mimetype='text/csv')

    # add a filename
    response.headers.set("Content-Disposition", "attachment", filename="data.csv")

    # delete data from db
    for instance in session.query(FourData).order_by(FourData.id):
        session.delete(instance)
        session.commit()
    return response


if __name__ == '__main__':
    # starting -> python app.py
    app.run(host="localhost", port=8080)